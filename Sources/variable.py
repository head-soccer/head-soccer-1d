import math
import pygame


# Initialise la bibliothèque Pygame
pygame.init()
pygame.mixer.init()
# Dimensions de l'écran
largeur_ecran, hauteur_ecran = 800, 600
choix = pygame.display.set_mode((largeur_ecran, hauteur_ecran))
principale = pygame.display.set_mode((largeur_ecran, hauteur_ecran))
ecran = pygame.display.set_mode((largeur_ecran, hauteur_ecran))
intro = pygame.display.set_mode((largeur_ecran, hauteur_ecran))
blanc = (255, 255, 255)
score_joueur1_precedent = 0
score_joueur2_precedent = 0
# Fonction pour redimensionner une image
def redimensionner_image(image, facteur):
    nouvelle_largeur = int(image.get_width() * facteur)
    nouvelle_hauteur = int(image.get_height() * facteur)
    return pygame.transform.scale(image, (nouvelle_largeur, nouvelle_hauteur))

# Affichage de l'écran de démarrage
splash_font = pygame.font.Font("PressStart2P-Regular.ttf", 30, bold=True)
nom_jeu_texte = splash_font.render("Head Soccer 1D", True, blanc)
createurs_texte = splash_font.render("Créé par DigiNSI", True, blanc)
menu = pygame.image.load("menu.png")
font = pygame.font.Font("PressStart2P-Regular.ttf", 100, bold=True)

# Chargement et redimensionnement de l'image du titre
titre = pygame.image.load("titre.png")
titre_redimensionne = redimensionner_image(titre, 0.5)
titre1 = pygame.image.load("title1.png")
titre_rect = titre_redimensionne.get_rect(center=(largeur_ecran // 2, hauteur_ecran // 4))
titre1_rect = titre_redimensionne.get_rect(center=(largeur_ecran // 2 - 65, hauteur_ecran // 4 + 120))

# Positionnement du texte au centre de l'écran
nom_jeu_rect = nom_jeu_texte.get_rect(center=(largeur_ecran // 2, hauteur_ecran // 2 - 50))
createurs_rect = createurs_texte.get_rect(center=(largeur_ecran // 2, hauteur_ecran // 2 + 50))

# Variables pour le tir parabolique
angle_tir_parabolique = 45
vitesse_tir_parabolique = 10
angle_tir_rad = math.radians(angle_tir_parabolique)
vitesse_x = vitesse_tir_parabolique * math.cos(angle_tir_rad)
vitesse_y = -vitesse_tir_parabolique * math.sin(angle_tir_rad)

# Chargement et redimensionnement des images de joueurs
joueur1_original = pygame.image.load("joueur_1.png")
joueur2_original = pygame.image.load("joueur_2.png")
joueur1 = redimensionner_image(joueur1_original, 0.7)
joueur2 = redimensionner_image(joueur2_original, 0.7)

# Obtenir la position rectangulaire des images
joueur1_rect = joueur1.get_rect(topleft=(50, hauteur_ecran // 2 - joueur1.get_height() // 2))
joueur2_rect = joueur2.get_rect(topleft=(largeur_ecran - 50 - joueur2.get_width(), hauteur_ecran // 2 - joueur2.get_height() // 2))

# Chargement des images de cage et de jambes
cage_gauche = pygame.image.load("BUT.png")
cage_droite = pygame.transform.flip(cage_gauche, True, False)
jambe_gauche = redimensionner_image(pygame.image.load("Jambes.png"), 1.1)
jambe_droite = redimensionner_image(pygame.image.load("Jambes.png"), 1.1)


# Positionnement des rectangles de but
cage_gauche_rect = cage_gauche.get_rect(topleft=(0, hauteur_ecran // 2 - cage_gauche.get_height() // 2 + 110))
cage_droite_rect = cage_droite.get_rect(topright=(largeur_ecran, hauteur_ecran // 2 - cage_droite.get_height() // 2 + 110))
# Redimensionnement des images de jambe
jambe_gauche_rect = jambe_gauche.get_rect()
jambe_droite_rect = jambe_droite.get_rect()


# Chargement de l'image de fond
fond = pygame.image.load("bg.png")

# Chargement et redimensionnement de l'image de la balle
balle = redimensionner_image(pygame.image.load("ballon.png"), 0.4)
balle_rect = balle.get_rect(center=(largeur_ecran // 2, hauteur_ecran // 2))
balle_vitesse = [8, 5]

# Variables pour les mouvements des joueurs
joueur_vitesse, joueur1_vitesse, joueur2_vitesse = 7, 0, 0

# Variables pour le saut des joueurs
gravite, jump_height, jumping1, jumping2 = 2, 25, False, False

# Variables pour le temps de jeu
temps_de_jeu_initial, temps_de_jeu_restant = 90, 90 * 60

# Variables pour le tir parabolique des joueurs
tir_parabolique_joueur1_active, tir_parabolique_joueur2_active = False, False
angle_tir_joueur1_rad, angle_tir_joueur2_rad = 0, 0
vitesse_x_joueur1, vitesse_y_joueur1, vitesse_x_joueur2, vitesse_y_joueur2 = 0, 0, 0, 0

# Variables pour le suivi des positions de la balle
positions_balle, max_positions = [], 20

# Variables pour le score des joueurs
score_joueur1, score_joueur2 = 0, 0

# Variable pour le rebond de la balle
rebond = None
max_y_rebond_precedent_joueur1 = 0
max_y_rebond_precedent_joueur2 = 0
joueur_collision = False
dist = 0
dist2 = 0
# Variable pour arrêter le jeu
arreter = False
# Limite de distance pour activer le tir parabolique
distance_limite = 50
# Initialisation de la clock de Pygame
horloge = pygame.time.Clock()

# Temps de démarrage
temps_de_demarrage = 2000
temps_actuel = pygame.time.get_ticks()
# Définir le début du jeu ici, avant d'entrer dans la boucle principale du jeu
temps_debut = pygame.time.get_ticks()
mi_temps_declenche = False
temps_debut_affichage_mi_temps = 0
# Rendre le texte
temps_affiche = 0
temps_texte = ""
texte_temps = font.render(temps_texte, True, (0, 0, 0))  # (0, 0, 0) représente la couleur noire
texte_rect = texte_temps.get_rect(midtop=(largeur_ecran // 2, 10))
font_scores = pygame.font.Font("PressStart2P-Regular.ttf", 50, bold=True)
# Temps de rotation
last_rotation_time_g = pygame.time.get_ticks()
last_rotation_time_d = pygame.time.get_ticks()

# Distance entre joueurs et balle
distance_joueur1_balle = abs(joueur1_rect.centerx - balle_rect.centerx)
distance_joueur2_balle = abs(joueur2_rect.centerx - balle_rect.centerx)
difference_top_bottom2 = abs(joueur2_rect.centery - balle_rect.centery)
# Classe pour les boutons
class Bouton:
    def __init__(self, x, y, largeur, hauteur, couleur_normale, couleur_survol, texte):
        self.rect = pygame.Rect(x, y, largeur, hauteur)
        self.couleur_normale = couleur_normale
        self.couleur_survol = couleur_survol
        self.texte = texte
        self.police = pygame.font.Font(None, 36)
        self.couleur_actuelle = self.couleur_normale

    def dessiner(self, surface, epaisseur_bordure=5):
        pygame.draw.rect(surface, self.couleur_actuelle, self.rect, border_radius=10)
        pygame.draw.rect(surface, (255, 255, 255), self.rect, border_radius=10, width=epaisseur_bordure)
        texte_surface = self.police.render(self.texte, True, (255, 255, 255))
        texte_rect = texte_surface.get_rect(center=self.rect.center)
        surface.blit(texte_surface, texte_rect)

    def survol_souris(self):
        return self.rect.collidepoint(pygame.mouse.get_pos())

# Définir les couleurs
COULEUR_BOUTON_NORMAL = (66, 81, 79)
COULEUR_BOUTON_SURVOL = (13, 35, 26)

# Définir les boutons
boutons = [
    Bouton((largeur_ecran - 200) // 2, hauteur_ecran - 100 - i * 60, 200, 50, COULEUR_BOUTON_NORMAL,
           COULEUR_BOUTON_SURVOL, text)
    for i, text in enumerate(["1 VS 1", "1 VS IA", "Personnalisation"])
]

supporteur = pygame.mixer.Sound("supporteur.mp3")
supporteur.set_volume(0.1)
sifflet = pygame.mixer.Sound("sifflet.mp3")
fait = False
indice = False
# Chargement des images des différentes équipes
Angleterre = pygame.image.load("Angleterre.jpg")
Espagne = pygame.image.load("Espagne.jpg")
France = pygame.image.load("France.jpg")

# Création de la fenêtre
window = pygame.display.set_mode((largeur_ecran, hauteur_ecran))
pygame.display.set_caption("Menu avec Pygame")

# Position des images des équipes
teams_images = [Angleterre, France, Espagne]
num_teams = len(teams_images)
num_columns = 3
gap_x = 100
gap_y = 200
image_width = teams_images[0].get_width()
image_height = teams_images[0].get_height()
x_start = (largeur_ecran - (num_columns * image_width + (num_columns - 1) * gap_x)) // 2
y_start = (hauteur_ecran - (num_teams // num_columns * image_height + (num_teams // num_columns - 1) * gap_y)) // 2

# Dimensions du carré de sélection
SELECTION_SIZE = 120

# Position initiale du carré de sélection du joueur 1
selection1_x = x_start + image_width // 2 - SELECTION_SIZE // 2
selection1_y = y_start + image_height // 2 - SELECTION_SIZE // 2

# Position initiale du carré de sélection du joueur 2
selection2_x = x_start + image_width + gap_x-SELECTION_SIZE // 4
selection2_y = y_start + image_height // 2 - SELECTION_SIZE // 2

# Charger une police de taille 36
font = pygame.font.Font(None, 36)

# Variable pour stocker le pays sélectionné par le joueur 1
selected_country1 = "Angleterre"

# Variable pour stocker le pays sélectionné par le joueur 2
selected_country2 = "Angleterre"

# Champ de saisie du nom du joueur 1
input_rect1 = pygame.Rect(largeur_ecran // 2 - 200, hauteur_ecran // 2 + 100, 400, 40)
player1_name = ''

# Champ de saisie du nom du joueur 2
input_rect2 = pygame.Rect(largeur_ecran // 2- 200, hauteur_ecran // 2 + 150, 400, 40)
player2_name = ''

def lire_audio(nom_fichier, volume):
    pygame.init()
    pygame.mixer.init()
    pygame.mixer.music.load(nom_fichier)
    pygame.mixer.music.set_volume(volume)
    # Réglez le volume
    pygame.mixer.music.play()

but = "sifflet.mp3"
volume = 0.5  # Volume entre 0.0 et 1.0