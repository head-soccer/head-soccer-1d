import pygame
from variable import *

def menu_jeu_football():
    # Initialisation de l'objet clock
    clock = pygame.time.Clock()

    # Initialisation de la fenêtre
    pygame.display.set_caption("Menu du jeu de football")

    # Police de caractères
    police = pygame.font.Font(None, 36)

    # Définition des touches par défaut pour le joueur 1 et le joueur 2
    touches_joueur1 = {
        "Sauter": pygame.K_z,
        "Tir bas": pygame.K_a,
        "Tir haut": pygame.K_s,
        "Droite": pygame.K_d,
        "Gauche": pygame.K_q
    }

    touches_joueur2 = {
        "Sauter": pygame.K_UP,
        "Tir bas": pygame.K_DOWN,
        "Tir haut": pygame.K_SPACE,
        "Droite": pygame.K_RIGHT,
        "Gauche": pygame.K_LEFT
    }

    touches_joueurs = [touches_joueur1, touches_joueur2]

    # Position des boutons
    boutons_rect_joueur1 = {
        "Sauter": pygame.Rect(150, 260, 200, 50),
        "Tir bas": pygame.Rect(150, 320, 200, 50),
        "Tir haut": pygame.Rect(150, 380, 200, 50),
        "Droite": pygame.Rect(150, 440, 200, 50),
        "Gauche": pygame.Rect(150, 500, 200, 50),
    }

    boutons_rect_joueur2 = {
        "Sauter": pygame.Rect(450, 260, 200, 50),
        "Tir bas": pygame.Rect(450, 320, 200, 50),
        "Tir haut": pygame.Rect(450, 380, 200, 50),
        "Droite": pygame.Rect(450, 440, 200, 50),
        "Gauche": pygame.Rect(450, 500, 200, 50),
    }

    boutons_rect_joueurs = [boutons_rect_joueur1, boutons_rect_joueur2]

    # Position des colonnes de sélection
    colonne_joueur1 = pygame.Rect(125, 260, 200, 250)
    colonne_joueur2 = pygame.Rect(425, 260, 200, 250)

    def afficher_menu():
        choix.blit(menu, (0, 0))
        titre = font_scores.render("Menu", True, blanc)
        choix.blit(titre, (largeur_ecran // 2 - titre.get_width() // 2, 50))

        # Ajout du texte pour quitter
        texte_quitter = splash_font.render("'Suppr' pour quitter", True, (255, 255, 255))
        choix.blit(texte_quitter, (largeur_ecran // 2 - texte_quitter.get_width() // 2, 150))

        for joueur, (touches, boutons_rect) in enumerate(zip(touches_joueurs, boutons_rect_joueurs)):
            # Affichage des titres des colonnes
            colonne_titre = splash_font.render(f"Joueur {joueur + 1}", True, blanc)
            choix.blit(colonne_titre, (colonne_joueur1.x + joueur * (colonne_joueur2.x - colonne_joueur1.x), colonne_joueur1.y - 40))

            for action, rect in boutons_rect.items():
                if rect.collidepoint(pygame.mouse.get_pos()):
                    pygame.draw.rect(choix, COULEUR_BOUTON_SURVOL, rect, border_radius=10)
                else:
                    pygame.draw.rect(choix, COULEUR_BOUTON_NORMAL, rect, border_radius=10)
                pygame.draw.rect(choix, (0, 0, 0), rect, 4, border_radius=10)
                texte = police.render(f"{action} - {pygame.key.name(touches[action]).lower().replace('_', '')}", True, blanc)
                choix.blit(texte, (rect.x + 10, rect.y + 10))

        pygame.display.flip()

    def changer_touche(action, joueur):
        attente_touche = True
        while attente_touche:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                elif event.type == pygame.KEYDOWN:
                    nouvelle_touche = event.key
                    # Vérifier si la nouvelle touche est déjà utilisée par un autre contrôle
                    touche_deja_utilisee = False
                    for autre_action, autre_touche in touches_joueur1.items():
                        if autre_action != action and autre_touche == nouvelle_touche:
                            touche_deja_utilisee = True
                            print("Cette touche est déjà utilisée pour un autre contrôle. Veuillez sélectionner une autre touche.")
                            break
                    for autre_action, autre_touche in touches_joueur2.items():
                        if autre_action != action and autre_touche == nouvelle_touche:
                            touche_deja_utilisee = True
                            print("Cette touche est déjà utilisée pour un autre contrôle. Veuillez sélectionner une autre touche.")
                            break
                    # Si la touche n'est pas déjà utilisée, l'assigner au contrôle
                    if not touche_deja_utilisee:
                        if joueur == 1:
                            touches_joueur1[action] = nouvelle_touche
                        elif joueur == 2:
                            touches_joueur2[action] = nouvelle_touche
                        attente_touche = False

    # Boucle principale
    running = True
    quitter = False

    while running and not quitter:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                for joueur, boutons_rect in enumerate(boutons_rect_joueurs):
                    for action, rect in boutons_rect.items():
                        if rect.collidepoint(event.pos):
                            changer_touche(action, joueur + 1)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_DELETE:
                    quitter = True  # Quitter la boucle

        # Effet hover sur les boutons
        for joueur, boutons_rect in enumerate(boutons_rect_joueurs):
            for action, rect in boutons_rect.items():
                if rect.collidepoint(pygame.mouse.get_pos()):
                    pygame.draw.rect(choix, blanc, rect, border_radius=10)

        afficher_menu()
    pygame.display.flip()
    clock.tick(60)
    return touches_joueurs

