import math
import pygame
from variable import *
from demarrage import *
from fonction import *
from menu import *
import random


def ia_decision_joueur1(joueur1_rect, joueur2_rect, balle_rect, hauteur_ecran, joueur_collision):
    global tir_parabolique_joueur2_active, tir_parabolique_joueur1_active, max_y_rebond_precedent_joueur1, vitesse_x_joueur1, vitesse_y_joueur1, jumping1, joueur1_vitesse
    global temps_balle_sur_tete # Nouvelle variable pour suivre le temps que la balle reste au-dessus du joueur
    global joueur2_rect_prev_x
    distance_x = abs(joueur1_rect.centerx - balle_rect.centerx)
    distance_y = abs(joueur1_rect.centery - balle_rect.centery)
    distance_minimale = 150  # Vous pouvez ajuster cette valeur en fonction de vos besoins
    # Mettre à jour la position précédente du joueur 2

    # Calculer la distance entre le joueur 2 et la balle
    distance_joueur2_balle = distance_entre_joueurs(joueur2_rect, balle_rect)
    # Si la distance entre les joueurs est inférieure à 150, s'arrêter
    if distance_entre_joueurs(joueur1_rect, joueur2_rect) < distance_minimale:
        # Si le joueur 2 avance vers la balle, reculer
        if joueur2_rect.x != joueur2_rect_prev_x:
            joueur1_rect.x -= (joueur_vitesse * 1.2)
    else:
        # Déplacer le joueur automatiquement en fonction de la position de la balle
            if balle_rect.centerx < joueur1_rect.centerx:
                joueur1_rect.x -= joueur_vitesse
            elif balle_rect.centerx > joueur1_rect.centerx:
                joueur1_rect.x += joueur_vitesse

    # Logique de saut si la balle est au-dessus du joueur
    if balle_rect.centery < 400 and not jumping1 and distance_x < 150 and not tir_parabolique_joueur1_active:
        joueur1_vitesse = -jump_height
        jumping1 = True
    if 1<=abs(balle_rect.centerx-joueur1_rect.centerx)<=6 and joueur1_rect.centery == 465:
        balle_rect.y = 600
        balle_rect.x +=40

    joueur2_rect_prev_x = joueur2_rect.x
    if (abs(cage_droite_rect.left - balle_rect.centerx) < 250 or abs(cage_gauche_rect.left - balle_rect.centerx)< 200 )and not jumping1:
        if (
                        not tir_parabolique_joueur1_active
                        and not tir_parabolique_joueur2_active
                        and abs(joueur1_rect.centerx - balle_rect.centerx) < 70
                        and joueur1_rect.centerx - balle_rect.centerx < 0
                        and abs(joueur1_rect.centery - balle_rect.centery) < 70
        ):
            facteur_hasard_tir = random.uniform(0, 100)
            if facteur_hasard_tir > 60:  # 90% de chance d'activer le tir parabolique
                tir_parabolique_joueur1_active = True
                angle_tir_joueur1_rad = math.radians(angle_tir_parabolique)
                vitesse_x_joueur1 = vitesse_tir_parabolique * math.sin(angle_tir_joueur1_rad) + 12
                vitesse_y_joueur1 = -vitesse_tir_parabolique * 3 * math.sin(angle_tir_joueur1_rad)  # Augmentez le coefficient pour une trajectoire plus prononcée



def boucle():
                        global tir_parabolique_joueur2_active, tir_parabolique_joueur1_active, max_y_rebond_precedent_joueur1, vitesse_x_joueur1, vitesse_y_joueur1, jumping1, joueur1_vitesse
                        global temps_balle_sur_tete # Nouvelle variable pour suivre le temps que la balle reste au-dessus du joueur
                        global joueur2_rect_prev_x, temps_de_jeu_restant, joueur1_rect, balle_rect, positions_balle, max_positions, largeur_ecran, joueur2_rect, balle_rect, positions_balle, max_positions, largeur_ecran
                        global joueur1_vitesse, joueur2_vitesse, jumping1, jumping2, touches, tir_parabolique_joueur2_active, jumping2, joueur2_rect, balle_rect, vitesse_x_joueur2, vitesse_y_joueur2, angle_tir_parabolique, vitesse_tir_parabolique
                        global tir_parabolique_joueur2_active, vitesse_x_joueur2, vitesse_y_joueur2, gravite, joueur_collision, max_y_rebond_precedent_joueur2, score_joueur1, score_joueur2, texte_score_joueur1, texte_score_joueur2, texte_temps, texte_rect
                        # Calcul des positions des joueurs et de la balle
                        pygame.mixer.init()
                        distance_joueur1_balle, positions_balle, difference_top_bottom1 = calculer_positions_joueurs_et_balle(
                            joueur1_rect, balle_rect, positions_balle, max_positions, largeur_ecran)
                        distance_joueur2_balle, positions_balle, difference_top_bottom2 = calculer_positions_joueurs_et_balle(
                            joueur2_rect, balle_rect, positions_balle, max_positions, largeur_ecran)

                        # Gestion des collisions entre les joueurs et la balle
                        gestion_collision_joueur_balle(touches_perso, joueur1_rect, balle_rect, balle_vitesse)
                        gestion_collision_joueur_balle(touches_perso, joueur2_rect, balle_rect, balle_vitesse)



                        # Appel de la fonction pour gérer les actions du joueur en fonction des touches pressées
                        gestion_touches_joueur2(touches_perso, touches, joueur2_rect, balle_rect,joueur_vitesse, jump_height, hauteur_ecran)
                        # Affichage de l'écran correspondant si la touche 'SPACE' est enfoncée
                        if touches[touches_perso[1]["Tir haut"]]:
                            afficher_ecran("droite", ecran, fond, positions_balle, jambe_gauche, jambe_droite, jambe_gauche_rect, jambe_droite_rect,
                                           joueur1, joueur2, balle, cage_gauche, cage_droite, texte_temps, texte_rect,
                                           texte_score_joueur1, texte_score_joueur2, blanc, max_positions, largeur_ecran, joueur1_rect, joueur2_rect, balle_rect, cage_droite_rect, cage_gauche_rect)

                        # Gestion du saut des joueurs et récupération des informations de saut
                        joueur2_vitesse, jumping2 = gerer_saut_joueurs2(touches_perso, touches, joueur2_vitesse, jump_height, jumping2)

                        # Gestion de la collision entre les joueurs
                        if joueur1_rect.colliderect(joueur2_rect):
                            joueur1_rect.x, joueur2_rect.x = (joueur1_rect.x - 10, joueur2_rect.x + 10) if joueur1_rect.x < joueur2_rect.x else (joueur1_rect.x + 10, joueur2_rect.x - 10)

                        # Mise à jour de la position verticale du joueur 1 et ajustement de la vitesse en fonction de la gravité
                        joueur1_rect.y += joueur1_vitesse
                        joueur1_vitesse += gravite

                        # Mise à jour de la position verticale du joueur 2 et ajustement de la vitesse en fonction de la gravité
                        joueur2_rect.y += joueur2_vitesse
                        joueur2_vitesse += gravite

                        # Limite du saut du joueur 1 pour éviter un déplacement infini vers le haut
                        if joueur1_rect.y >= hauteur_ecran // 2 - joueur1_rect.height // 2 + 165:
                            joueur1_rect.y = hauteur_ecran // 2 - joueur1_rect.height // 2 + 165
                            jumping1 = False

                        # Limite du saut du joueur 2 pour éviter un déplacement infini vers le haut
                        if joueur2_rect.y >= hauteur_ecran // 2 - joueur2_rect.height // 2 + 165:
                            joueur2_rect.y = hauteur_ecran // 2 - joueur2_rect.height // 2 + 165
                            jumping2 = False



                        # Activation du tir parabolique pour le joueur 2 et récupération des vitesses initiales
                        tir_parabolique_joueur2_active, vitesse_x_joueur2, vitesse_y_joueur2 = activer_tir_parabolique(touches_perso,
                            touches, tir_parabolique_joueur2_active, jumping2, joueur2_rect, balle_rect, vitesse_x_joueur2, vitesse_y_joueur2, angle_tir_parabolique, vitesse_tir_parabolique
                        )

                        # Gestion du tir parabolique du joueur 1 et mise à jour des vitesses
                        tir_parabolique_joueur1_active, vitesse_x_joueur1, vitesse_y_joueur1, max_y_rebond_precedent_joueur1 = gestion_tir_parabolique(
                            tir_parabolique_joueur1_active, vitesse_x_joueur1, vitesse_y_joueur1, gravite, joueur_collision, max_y_rebond_precedent_joueur1,
                            balle_rect, joueur1_rect, joueur2_rect, distance_joueur2_balle, difference_top_bottom2, dist, distance_joueur1_balle, balle_vitesse, hauteur_ecran, dist2,tir_parabolique_joueur1_active
                        )

                        # Gestion du tir parabolique du joueur 2 et mise à jour des vitesses
                        tir_parabolique_joueur2_active, vitesse_x_joueur2, vitesse_y_joueur2, max_y_rebond_precedent_joueur2 = gestion_tir_parabolique(
                            tir_parabolique_joueur2_active, vitesse_x_joueur2, vitesse_y_joueur2, gravite, joueur_collision, max_y_rebond_precedent_joueur2,
                            balle_rect, joueur2_rect, joueur1_rect, distance_joueur1_balle, difference_top_bottom1, dist2, distance_joueur2_balle, balle_vitesse, hauteur_ecran, dist,tir_parabolique_joueur1_active
                        )

                        # Vérification et ajustement de la position de la balle en fonction des limites de l'écran
                        balle_rect.y = max(0, min(balle_rect.y, hauteur_ecran // 2 - balle_rect.height // 2 + 200))

                        # Mise à jour de la position verticale de la balle avec gravité et logique de rebond
                        balle_rect.y += balle_vitesse[1]
                        balle_vitesse[1] += gravite

                        # Vérification de la collision avec le sol et inversion de la direction verticale si nécessaire
                        if balle_rect.bottom >= hauteur_ecran // 2 - balle_rect.height // 2 + 200:
                            balle_rect.bottom = hauteur_ecran // 2 - balle_rect.height // 2 + 200

                            # Ajustement du coefficient de restitution en fonction de la vitesse verticale
                            vitesse_verticale = abs(balle_vitesse[1])
                            coefficient_restitution = 0.6  # Ajustez ce coefficient selon vos besoins

                            # Si la vitesse est trop petite, désactivez les rebonds
                            balle_vitesse[1] = 0 if vitesse_verticale < 10.0 else -vitesse_verticale * coefficient_restitution

                        # Vérifie si la balle a traversé la cage gauche
                        if (cage_gauche_rect.top < balle_rect.centery < cage_gauche_rect.bottom):
                            # Vérifie si la balle est entre le haut et le bas de la cage gauche
                            # et si la balle a dépassé le côté droit de la cage gauche de plus de 52 pixels
                            if balle_rect.top > cage_gauche_rect.top and balle_rect.bottom < cage_gauche_rect.bottom and (balle_rect.left - cage_gauche_rect.right) < -52:
                                print("But du joueur 2!")
                                # Réinitialiser la position de la balle
                                balle_rect.center = (largeur_ecran // 2, hauteur_ecran // 2)
                                tir_parabolique_joueur1_active = False
                                tir_parabolique_joueur2_active = False
                                # Réinitialiser la position des joueurs
                                joueur1_rect = joueur1.get_rect(topleft=(50, hauteur_ecran // 2 - joueur1.get_height() // 2))
                                joueur2_rect = joueur2.get_rect(topleft=(largeur_ecran - 50 - joueur2.get_width(), hauteur_ecran // 2 - joueur2.get_height() // 2))
                                score_joueur2 += 1
                                lire_audio(but,volume)

                        # Vérifie si la balle a traversé la cage droite
                        if (cage_droite_rect.top < balle_rect.centery < cage_droite_rect.bottom) :
                            # Vérifie si la balle est entre le haut et le bas de la cage droite
                            # et si la balle a dépassé le côté gauche de la cage droite de plus de 52 pixels
                            if balle_rect.top > cage_droite_rect.top and balle_rect.bottom < cage_droite_rect.bottom and (-balle_rect.right + cage_droite_rect.left) < -52:
                                print("But du joueur 1!")
                                # Réinitialiser la position de la balle
                                balle_rect.center = (largeur_ecran // 2, hauteur_ecran // 2)
                                tir_parabolique_joueur1_active = False
                                tir_parabolique_joueur2_active = False
                                # Réinitialiser la position des joueurs
                                joueur1_rect = joueur1.get_rect(topleft=(50, hauteur_ecran // 2 - joueur1.get_height() // 2))
                                joueur2_rect = joueur2.get_rect(topleft=(largeur_ecran - 50 - joueur2.get_width(), hauteur_ecran // 2 - joueur2.get_height() // 2))
                                score_joueur1 += 1
                                lire_audio(but,volume)

                        # Vérifier si la balle atteint le bord gauche ou droit de l'écran
                        if balle_rect.left <= 0 or balle_rect.right >= largeur_ecran:
                            # Téléporter la balle au milieu du terrain
                            balle_rect.center = (largeur_ecran // 2, hauteur_ecran // 2)
                            # Désactiver les tirs paraboliques des deux joueurs
                            tir_parabolique_joueur1_active = tir_parabolique_joueur2_active = False

                        # Vérifier la collision entre le joueur 1 et la balle, avec un léger décalage permis pour le joueur 2
                        if joueur1_rect.colliderect(balle_rect) and abs(joueur2_rect.left - balle_rect.right) < 5:
                            # Ajuster la position verticale de la balle vers le haut
                            balle_rect.y -= 10
                            # Inverser la direction verticale de la balle
                            balle_vitesse[1] = -abs(balle_vitesse[1])

                            # Ajuster la position des joueurs en fonction de la position de la balle
                            offset = 20 if joueur1_rect.x < joueur2_rect.x else -20
                            joueur1_rect.x += offset
                            joueur2_rect.x -= offset

                        # Vérifier la collision entre le joueur 1 et la balle ainsi que la collision entre le joueur 2 et la balle
                        if joueur1_rect.colliderect(balle_rect) and joueur2_rect.colliderect(balle_rect):
                            # Ajuster la position verticale de la balle vers le haut
                            balle_rect.y -= 10
                            # Inverser la direction verticale de la balle
                            balle_vitesse[1] = -abs(balle_vitesse[1])

                        # Calculer les distances entre la partie supérieure des joueurs et la partie inférieure de la balle
                        dist_joueur1 = abs(joueur1_rect.top - balle_rect.bottom)
                        dist_joueur2 = abs(joueur2_rect.top - balle_rect.bottom)

                        # Vérifier les conditions pour un rebond de la balle sur la partie supérieure des joueurs
                        if (
                            # Si la distance entre la partie supérieure du joueur 1 et la partie inférieure de la balle est petite
                            (dist_joueur1 < 10 and distance_joueur1_balle < 15 or
                             # Ou si la vitesse verticale de la balle est élevée et la distance est modérée
                             (balle_vitesse[1] > 32 and dist_joueur1 < 30 and distance_joueur1_balle < 15))
                            # Ou si la distance entre la partie supérieure du joueur 2 et la partie inférieure de la balle est petite
                            or (dist_joueur2 < 10 and distance_joueur2_balle < 15 or
                                # Ou si la vitesse verticale de la balle est élevée et la distance est modérée
                                (balle_vitesse[1] > 32 and dist_joueur2 < 30 and distance_joueur2_balle < 15))
                        ) and not (tir_parabolique_joueur1_active or tir_parabolique_joueur2_active):
                            # Coefficient de rebond
                            bounce_coefficient = 0.5
                            # Inverser la direction verticale de la balle avec un coefficient de rebond
                            balle_vitesse[1] = -abs(balle_vitesse[1] * bounce_coefficient)

                        # Limiter la position horizontale du joueur 1 dans les limites de l'écran
                        joueur1_rect.x = max(0, min(largeur_ecran - joueur1_rect.width, joueur1_rect.x))
                        # Limiter la position horizontale du joueur 2 dans les limites de l'écran
                        joueur2_rect.x = max(0, min(largeur_ecran - joueur2_rect.width, joueur2_rect.x))

                        # Décrémenter le temps dejeu restant
                        temps_de_jeu_restant -= 1

                        # Convertir le temps restant en minutes
                        temps_affiche = temps_de_jeu_restant // 60

                        # Convertir le temps affiché en chaîne de caractères
                        temps_texte = str(temps_affiche)

                        # Enregistrer la position actuelle de la balle pour créer une traînée
                        traînée_position = (balle_rect.centerx, balle_rect.centery)
                        font_scores = pygame.font.Font("PressStart2P-Regular.ttf", 50, bold=True)
                        # Rendre le texte du temps avec la police définie
                        texte_temps = font_scores.render(temps_texte, True, (0, 0, 0))  # (0, 0, 0) représente la couleur noire

                        # Obtenir la position du texte pour le centrer horizontalement en haut de l'écran
                        texte_rect = texte_temps.get_rect(midtop=(largeur_ecran // 2, 10))

                        # Charger une police spécifique pour les scores
                        font_scores = pygame.font.Font("PressStart2P-Regular.ttf", 50, bold=True)

                        # Rendre les textes des scores pour les joueurs 1 et 2
                        texte_score_joueur1 = font_scores.render(str(score_joueur1), True, (255, 255, 255))
                        texte_score_joueur2 = font_scores.render(str(score_joueur2), True, (255, 255, 255))


                        # Positionner les images des jambes en bas à gauche et à droite des joueurs respectifs en tenant compte du saut
                        jambe_droite_rect.topleft = (joueur1_rect.left + 25, min(joueur1_rect.bottom + joueur1_vitesse, hauteur_ecran - jambe_droite_rect.height-95))
                        jambe_gauche_rect.topleft = (joueur2_rect.left, min(joueur2_rect.bottom - 20 + joueur2_vitesse, hauteur_ecran - jambe_gauche_rect.height -95))


# Initialisation de Pygame
pygame.init()

# Affichage de l'écran de démarrage
afficher_ecran_demarrage(intro, temps_de_demarrage, nom_jeu_texte, nom_jeu_rect, createurs_texte, createurs_rect)
# Appel de la fonction
touches_perso = menu_jeu_football()
# Initialisation de Pygame
pygame.init()
# Boucle principale du jeu
while True:
    pygame.mixer.stop()
    temps_actuel = pygame.time.get_ticks()
    temps_ecoule = temps_actuel - temps_debut
    # Gestion des événements
    for a in pygame.event.get():
        # Quitter le jeu si la fenêtre est fermée
        if a.type == pygame.QUIT:
            pygame.quit()
        # Gérer les clics de souris
        if a.type == pygame.MOUSEBUTTONDOWN and a.button == 1:
            # Vérifier quel bouton a été cliqué
            for bouton in boutons:
                indice = False
                if bouton.rect.collidepoint(a.pos) and bouton.texte == "1 VS 1":
                    temps_de_jeu_restant = 90*60
                    pygame.mixer.stop()
                    pygame.mixer.init()
                    supporteur.play()
                    while temps_de_jeu_restant > 0:
                        for z in pygame.event.get():
                            # Quitter le jeu si la fenêtre est fermée
                            if z.type == pygame.QUIT:
                                pygame.quit()
                            elif z.type == pygame.KEYDOWN and z.key == pygame.K_m:
                                # Réinitialiser le jeu
                                score_joueur1, score_joueur2, temps_de_jeu_restant, joueur2_rect, joueur1_rect, tir_parabolique_joueur1_active, tir_parabolique_joueur2_active, balle_rect = reinitialiser_jeu()
                                # Réinitialiser le temps de début et la variable mi_temps_declenche
                                temps_debut = pygame.time.get_ticks()
                                mi_temps_declenche = False
                                temps_de_jeu_restant = 0  # ou toute autre condition pour sortir de la boucle du jeu
                                indice = True

                        # Mise à jour des scores précédents des joueurs
                        score_joueur1_precedent = score_joueur1
                        score_joueur2_precedent = score_joueur2
                        # Gestion des touches du clavier
                        touches = pygame.key.get_pressed()
                        boucle()
                        # Activation du tir parabolique pour le joueur 1 et récupération des vitesses initiales
                        tir_parabolique_joueur1_active, vitesse_x_joueur1, vitesse_y_joueur1 = activer_tir_parabolique( touches_perso,
                            touches, tir_parabolique_joueur1_active, jumping1, joueur1_rect, balle_rect, vitesse_x_joueur1, vitesse_y_joueur1, angle_tir_parabolique, vitesse_tir_parabolique
                        )
                        # Affichage de l'écran correspondant si la touche 'S' est enfoncée
                        if touches[touches_perso[0]["Tir haut"]]:
                            afficher_ecran("gauche", ecran, fond, positions_balle, jambe_gauche, jambe_droite, jambe_gauche_rect, jambe_droite_rect,
                                           joueur1, joueur2, balle, cage_gauche, cage_droite, texte_temps, texte_rect,
                                           texte_score_joueur1, texte_score_joueur2, blanc, max_positions, largeur_ecran, joueur1_rect, joueur2_rect, balle_rect, cage_droite_rect, cage_gauche_rect)
                        joueur1_vitesse, jumping1 = gerer_saut_joueurs1(touches_perso, touches, joueur1_vitesse, jump_height, jumping1)
                        gestion_touches_joueur1(touches_perso, touches, joueur1_rect, balle_rect, joueur_vitesse, jump_height, hauteur_ecran)
                        # Remplir l'écran avec la couleur blanche
                        ecran.fill(blanc)
                        # Afficher l'image du fond
                        ecran.blit(fond, (0, 0))

                        # Afficher les traînées de la balle en fonction de ses positions antérieures
                        for i, (position, camp) in enumerate(positions_balle):
                            # Calculer la taille du cercle en fonction de sa position dans la liste
                            radius = int((10 / max_positions) * i)

                            # Changer la couleur en fonction du camp de la balle
                            color = (255, 77, 77, 50) if camp == "cote_gauche" else (50, 255, 255, 50)

                            # Dessiner un cercle sur l'écran avec la couleur et la position calculées
                            pygame.draw.circle(ecran, color, position, radius)

                        # Afficher les images des jambes des joueurs sur l'écran
                        ecran.blit(jambe_droite, jambe_droite_rect)
                        ecran.blit(jambe_gauche, jambe_gauche_rect)

                        # Afficher tous les éléments du jeu (joueurs, balle, cages, texte) sur l'écran
                        afficher(ecran, joueur1, joueur2, balle, cage_gauche, cage_droite, texte_temps, texte_rect,
                                texte_score_joueur1, texte_score_joueur2, largeur_ecran, joueur1_rect, joueur2_rect,
                                balle_rect, cage_droite_rect, cage_gauche_rect)
                        if temps_de_jeu_restant/60 <= 45 and not mi_temps_declenche and not fait and temps_de_jeu_restant >60:

                            Fait = False

                            pygame.mixer.stop()

                            sifflet.play()

                            temps_debut_affichage_mi_temps = 0

                            # Pendant la période de mi-temps
                            while temps_debut_affichage_mi_temps <= 3000:
                                temps_debut_affichage_mi_temps += 2
                                ecran.fill((0, 0, 0))
                                # Rendre le texte "Mi-Temps" avec une taille de police plus grande
                                mi_temps_texte = font_scores.render("MI-TEMPS", True, (255, 255, 255))  # Couleur blanche
                                mi_temps_texte = pygame.font.Font("PressStart2P-Regular.ttf", 100).render("MI-TEMPS", True, (255, 255, 255))  # Taille de police plus grande

                                # Obtenir le rectangle du texte pour le positionner au milieu de l'écran
                                mi_temps_rect = mi_temps_texte.get_rect(center=(largeur_ecran // 2, hauteur_ecran // 2))

                                # Afficher le texte "Mi-Temps" au milieu de l'écran
                                ecran.blit(mi_temps_texte, mi_temps_rect)

                                # Mettre à jour l'affichage
                                pygame.display.flip()

                                # Réinitialiser la position de la balle et les tirs paraboliques
                                balle_rect.center = (largeur_ecran // 2, hauteur_ecran // 2)
                                tir_parabolique_joueur1_active = False
                                tir_parabolique_joueur2_active = False


                                # Réinitialiser la position des joueurs

                                joueur1_rect = joueur1.get_rect(topleft=(50, hauteur_ecran // 2 - joueur1.get_height() // 2))

                                joueur2_rect = joueur2.get_rect(topleft=(largeur_ecran - 50 - joueur2.get_width(), hauteur_ecran // 2 - joueur2.get_height() // 2))

                                fait  = True

                                temps_de_jeu_restant = 45*60

                    if temps_de_jeu_restant <= 0 and not indice:
                        ecran.fill((0, 0, 0))  # Fond noir pour l'écran de fin
                        # Déterminer le vainqueur en fonction des scores
                        if score_joueur1 > score_joueur2:
                            vainqueur_texte = "Joueur 1"
                        elif score_joueur2 > score_joueur1:
                            vainqueur_texte = "Joueur 2"
                        else:
                            vainqueur_texte = "Match nul"

                        # Afficher le texte du vainqueur
                        vainqueur_font = pygame.font.Font(None, 36)
                        vainqueur_surface = vainqueur_font.render("Le vainqueur est : " + vainqueur_texte, True, (255, 255, 255))
                        vainqueur_rect = vainqueur_surface.get_rect(center=(largeur_ecran // 2, hauteur_ecran // 2))
                        ecran.blit(vainqueur_surface, vainqueur_rect)
                        # Mettre à jour l'affichage
                        pygame.display.flip()

                        # Attendre quelques secondes avant de quitter
                        pygame.time.wait(5000)  # Attendre 5 secondes avant de quitter


                    # Boucle de jeu principal
                elif bouton.rect.collidepoint(a.pos) and bouton.texte == "1 VS IA":
                    pygame.mixer.stop()
                    pygame.mixer.init()
                    supporteur.play()
                    temps_de_jeu_restant = 90*60
                    while temps_de_jeu_restant > 0:
                        temps_actuel = pygame.time.get_ticks()
                        temps_ecoule = temps_actuel - temps_debut

                        for e in pygame.event.get():
                            # Quitter le jeu si la fenêtre est fermée
                            if e.type == pygame.QUIT:
                                pygame.quit()
                            elif e.type == pygame.KEYDOWN and e.key == pygame.K_m:
                                # Réinitialiser le jeu
                                score_joueur1, score_joueur2, temps_de_jeu_restant, joueur2_rect, joueur1_rect, tir_parabolique_joueur1_active, tir_parabolique_joueur2_active, balle_rect = reinitialiser_jeu()
                                # Réinitialiser le temps de début et la variable mi_temps_declenche
                                temps_debut = pygame.time.get_ticks()
                                mi_temps_declenche = False
                                temps_de_jeu_restant = 0  # ou toute autre condition pour sortir de la boucle du jeu
                                indice = True
                        touches = pygame.key.get_pressed()
                        boucle()
                        # Remplir l'écran avec la couleur blanche
                        ecran.fill(blanc)

                        # Afficher l'image du fond
                        ecran.blit(fond, (0, 0))

                        # Afficher les traînées de la balle en fonction de ses positions antérieures
                        for i, (position, camp) in enumerate(positions_balle):
                            # Calculer la taille du cercle en fonction de sa position dans la liste
                            radius = int((10 / max_positions) * i)

                            # Changer la couleur en fonction du camp de la balle
                            color = (255, 77, 77, 50) if camp == "cote_gauche" else (50, 255, 255, 50)

                            # Dessiner un cercle sur l'écran avec la couleur et la position calculées
                            pygame.draw.circle(ecran, color, position, radius)

                        # Afficher les images des jambes des joueurs sur l'écran
                        ecran.blit(jambe_droite, jambe_droite_rect)
                        ecran.blit(jambe_gauche, jambe_gauche_rect)

                        # Afficher tous les éléments du jeu (joueurs, balle, cages, texte) sur l'écran
                        afficher(ecran, joueur1, joueur2, balle, cage_gauche, cage_droite, texte_temps, texte_rect,
                                texte_score_joueur1, texte_score_joueur2, largeur_ecran, joueur1_rect, joueur2_rect,
                                balle_rect, cage_droite_rect, cage_gauche_rect)
                        ia_decision_joueur1(joueur1_rect, joueur2_rect, balle_rect, hauteur_ecran, joueur_collision)
                        if temps_de_jeu_restant/60 <= 45 and not mi_temps_declenche and not fait and temps_de_jeu_restant >60:
                            Fait = False
                            pygame.mixer.stop()
                            sifflet.play()
                            temps_debut_affichage_mi_temps = 0
                            while temps_debut_affichage_mi_temps <= 3000:
                                temps_debut_affichage_mi_temps += 2
                                ecran.fill((0, 0, 0))
                                mi_temps_texte = font.render("Mi-temps", True, (255, 255, 255))
                                mi_temps_rect = mi_temps_texte.get_rect(midbottom=(largeur_ecran // 2, hauteur_ecran // 2 - 50))
                                ecran.blit(mi_temps_texte, mi_temps_rect)
                                pygame.display.flip()  # Mettre à jour l'affichage
                                balle_rect.center = (largeur_ecran // 2, hauteur_ecran // 2)
                                tir_parabolique_joueur1_active = False
                                tir_parabolique_joueur2_active = False
                                # Réinitialiser la position des joueurs
                                joueur1_rect = joueur1.get_rect(topleft=(50, hauteur_ecran // 2 - joueur1.get_height() // 2))
                                joueur2_rect = joueur2.get_rect(topleft=(largeur_ecran - 50 - joueur2.get_width(), hauteur_ecran // 2 - joueur2.get_height() // 2))
                                fait  = True
                                temps_de_jeu_restant = 45*60
                    if temps_de_jeu_restant <= 0 and not indice:
                        ecran.fill((0, 0, 0))  # Fond noir pour l'écran de fin
                        # Déterminer le vainqueur en fonction des scores
                        if score_joueur1 > score_joueur2:
                            vainqueur_texte = "Joueur 1"
                        elif score_joueur2 > score_joueur1:
                            vainqueur_texte = "Joueur 2"
                        else:
                            vainqueur_texte = "Match nul"

                        # Afficher le texte du vainqueur
                        vainqueur_font = pygame.font.Font(None, 36)
                        vainqueur_surface = vainqueur_font.render("Le vainqueur est : " + vainqueur_texte, True, (255, 255, 255))
                        vainqueur_rect = vainqueur_surface.get_rect(center=(largeur_ecran // 2, hauteur_ecran // 2))
                        ecran.blit(vainqueur_surface, vainqueur_rect)
                        # Mettre à jour l'affichage
                        pygame.display.flip()

                        # Attendre quelques secondes avant de quitter
                        pygame.time.wait(5000)  # Attendre 5 secondes avant de quitter



                elif bouton.rect.collidepoint(a.pos) and bouton.texte == "Personnalisation":

                    # Initialisation de Pygame
                    pygame.init()
                    # Fonction pour dessiner le menu
                    def draw_menu():
                        # Nettoyer l'écran
                        window.blit(menu, (0, 0))

                        # Dessiner les images des équipes
                        for i, image in enumerate(teams_images):
                            row = i // num_columns
                            col = i % num_columns
                            image_x = x_start + col * (image_width + gap_x)
                            image_y = y_start + row * (image_height + gap_y)
                            window.blit(image, (image_x, image_y))

                        # Dessiner le carré de sélection du joueur 1
                        pygame.draw.rect(window, (0, 0, 0), (selection1_x, selection1_y, SELECTION_SIZE, SELECTION_SIZE), 4)

                        # Dessiner le carré de sélection du joueur 2
                        pygame.draw.rect(window, (255, 0, 0), (selection2_x, selection2_y, SELECTION_SIZE, SELECTION_SIZE), 4)

                        # Afficher le nom du pays sélectionné par le joueur 1
                        if selected_country1 is not None:
                            text_surface = font.render(selected_country1, True, (0, 0, 0))
                            text_rect = text_surface.get_rect()
                            text_rect.center = (largeur_ecran//3, image_y+100)
                            window.blit(text_surface, text_rect)

                        # Afficher le nom du pays sélectionné par le joueur 2
                        if selected_country2 is not None:
                            text_surface = font.render(selected_country2, True, (255, 0, 0))
                            text_rect = text_surface.get_rect()
                            text_rect.center = (largeur_ecran *2//3, image_y+100)
                            window.blit(text_surface, text_rect)

                        # Afficher le champ de saisie du nom du joueur 1
                        pygame.draw.rect(window, (0, 0, 0), input_rect1, 2)
                        text_surface = font.render(player1_name, True, (0, 0, 0))
                        window.blit(text_surface, (input_rect1.x + 5, input_rect1.y + 5))
                        # Afficher le nom du joueur 1 au-dessus de son champ de saisie
                        text_surface = font.render("Joueur 1", True, (0, 0, 0))
                        text_rect = text_surface.get_rect()
                        text_rect.center = (largeur_ecran // 3-150, hauteur_ecran // 2 + 125)
                        window.blit(text_surface, text_rect)

                        # Afficher le champ de saisie du nom du joueur 2
                        pygame.draw.rect(window, (255, 0, 0), input_rect2, 2)
                        text_surface = font.render(player2_name, True, (255, 0, 0))
                        window.blit(text_surface, (input_rect2.x + 5, input_rect2.y + 5))

                        # Afficher le nom du joueur 2 au-dessus de son champ de saisie
                        text_surface = font.render("Joueur 2", True, (255, 0, 0))
                        text_rect = text_surface.get_rect()
                        text_rect.center = (largeur_ecran// 3-150, hauteur_ecran // 2 + 175)
                        window.blit(text_surface, text_rect)
                        # Afficher "Sélectionnez un pays" pour le joueur 1
                        text_surface = font.render("Sélectionnez un pays", True, (0, 0, 0))
                        text_rect = text_surface.get_rect()
                        text_rect.center = (largeur_ecran // 2, hauteur_ecran//2-100)
                        window.blit(text_surface, text_rect)

                        # Afficher le champ de saisie du nom du joueur 1
                        pygame.draw.rect(window, (0, 0, 0), input_rect1, 2)
                        text_surface = font.render(player1_name, True, (0, 0, 0))
                        window.blit(text_surface, (input_rect1.x + 5, input_rect1.y + 5))

                        # Afficher le champ de saisie du nom du joueur 2
                        pygame.draw.rect(window, (255, 0, 0), input_rect2, 2)
                        text_surface = font.render(player2_name, True, (255, 0, 0))
                        window.blit(text_surface, (input_rect2.x + 5, input_rect2.y + 5))


                    # Boucle principale
                    running = True
                    current_player = 1  # Variable pour suivre le joueur actuel (1 ou 2)
                    while running:
                        for event in pygame.event.get():
                            if event.type == pygame.QUIT:
                                running = False
                            elif event.type == pygame.MOUSEBUTTONDOWN:
                                # Récupérer les coordonnées du clic de souris
                                mouse_x, mouse_y = pygame.mouse.get_pos()
                                # Vérifier si le clic est à l'intérieur d'une image de club/pays pour le joueur 1
                                for i, image in enumerate(teams_images):
                                    row = i // num_columns
                                    col = i % num_columns
                                    image_x = x_start + col * (image_width + gap_x)
                                    image_y = y_start + row * (image_height + gap_y)
                                    if image_x <= mouse_x <= image_x + image_width and image_y <= mouse_y <= image_y + image_height:
                                        if current_player == 1:
                                            selection1_x = image_x + image_width // 2 - SELECTION_SIZE // 2
                                            selection1_y = image_y + image_height // 2 - SELECTION_SIZE // 2
                                            selected_country1 = ["Angleterre", "France", "Espagne"][i]
                                            current_player = 2  # Passer au joueur 2 après que le joueur 1 a sélectionné un pays
                                        elif current_player == 2:
                                            selection2_x = image_x + image_width // 2 - SELECTION_SIZE // 2
                                            selection2_y = image_y + image_height // 2 - SELECTION_SIZE // 2
                                            selected_country2 = ["Angleterre", "France", "Espagne"][i]
                                            current_player = 1  # Passer au joueur 1 après que le joueur 2 a sélectionné un pays
                                # Vérifier si le clic est à l'intérieur de la zone de texte du joueur 1
                                if input_rect1.collidepoint(mouse_x, mouse_y):
                                    current_player = 1
                                # Vérifier si le clic est à l'intérieur de la zone de texte du joueur 2
                                elif input_rect2.collidepoint(mouse_x, mouse_y):
                                    current_player = 2
                            elif event.type == pygame.KEYDOWN:
                                # Gestion des événements liés aux inputs
                                if event.key == pygame.K_BACKSPACE:
                                    if current_player == 1:
                                        player1_name = player1_name[:-1]
                                    elif current_player == 2:
                                        player2_name = player2_name[:-1]
                                elif event.key == pygame.K_DELETE:
                                        # Sauvegarder le nom du joueur 1 et le pays sélectionné
                                        print("Nom du joueur 1:", player1_name)
                                        print("Pays sélectionné:", selected_country1)
                                        # Sauvegarder le nom du joueur 2 et le pays sélectionné
                                        print("Nom du joueur 2:", player2_name)
                                        print("Pays sélectionné:", selected_country2)
                                        running = False
                                else:
                                    if current_player == 1:
                                        player1_name += event.unicode
                                    elif current_player == 2:
                                        player2_name += event.unicode

                        # Dessiner le menu
                        draw_menu()

                        # Mettre à jour l'affichage
                        pygame.display.flip()

    principale.fill((0, 0, 0))  # Fond noir

    # Changer la couleur des boutons au survol de la souris
    for bouton in boutons:
        if bouton.survol_souris():
            bouton.couleur_actuelle = COULEUR_BOUTON_SURVOL
        else:
            bouton.couleur_actuelle = COULEUR_BOUTON_NORMAL

    principale.blit(menu, (0, 0))
    principale.blit(titre_redimensionne, titre_rect)
    principale.blit(titre1, titre1_rect)
    for bouton in boutons:
        bouton.dessiner(principale, epaisseur_bordure=5)

    pygame.display.flip()
    pygame.time.Clock().tick(60)


