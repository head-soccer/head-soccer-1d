import pygame
import math
from variable import *

def calculer_positions_joueurs_et_balle(joueur_rect, balle_rect, positions_balle, max_positions, largeur_ecran):
    # Calcul des distances entre le joueur et la balle
    distance_joueur_balle = abs(joueur_rect.centerx - balle_rect.centerx)

    # Ajout de la position de la balle à la liste
    cote_balle = "cote_gauche" if balle_rect.centerx < largeur_ecran // 2 else "cote_droite"
    positions_balle.append((balle_rect.center, cote_balle))

    # Limiter la longueur de la liste des positions
    positions_balle = positions_balle[-max_positions:]

    # Calcul des différences de hauteur entre le joueur et la balle
    difference_top_bottom = abs(joueur_rect.centery - balle_rect.centery)

    return distance_joueur_balle, positions_balle, difference_top_bottom

def gestion_collision_joueur_balle(touches_perso, joueur_rect, balle_rect, balle_vitesse):
    # Vérifie s'il y a une collision entre le joueur et la balle
    if joueur_rect.colliderect(balle_rect):
        # Ajuste la direction de la vitesse de la balle en fonction de la position de la collision
        # Si la collision se produit du côté droit du joueur, la composante horizontale de la vitesse devient positive
        # Sinon, la composante horizontale de la vitesse devient négative
        balle_vitesse[0] = abs(balle_vitesse[0]) * (1 if balle_rect.x - joueur_rect.x > 0 else -1)

        # Applique la nouvelle position horizontale de la balle après la collision
        balle_rect.x += balle_vitesse[0]
    if joueur1_rect.y >= hauteur_ecran // 2 - joueur1_rect.height // 2 + 165:
        joueur1_rect.y = hauteur_ecran // 2 - joueur1_rect.height // 2 + 165
        jumping1 = False

    # Limite du saut du joueur 2 pour éviter un déplacement infini vers le haut
    if joueur2_rect.y >= hauteur_ecran // 2 - joueur2_rect.height // 2 + 165:
        joueur2_rect.y = hauteur_ecran // 2 - joueur2_rect.height // 2 + 165
        jumping2 = False
def gestion_touches_joueur1(touches_perso,touches, joueur1_rect, balle_rect, joueur_vitesse, jump_height, hauteur_ecran):
    # Gestion des déplacements du joueur 1 vers la gauche
    if touches[touches_perso[0]["Gauche"]] and joueur1_rect.top > 0 and not joueur1_rect.colliderect(balle_rect) and not joueur1_rect.colliderect(joueur2_rect):
        joueur1_rect.x -= joueur_vitesse

    # Gestion des déplacements du joueur 1 vers la droite
    if touches[touches_perso[0]["Droite"]] and joueur1_rect.bottom < hauteur_ecran and not joueur1_rect.colliderect(balle_rect) and not joueur1_rect.colliderect(joueur2_rect):
        joueur1_rect.x += joueur_vitesse
    if joueur1_rect.y >= hauteur_ecran // 2 - joueur1_rect.height // 2 + 165:
        joueur1_rect.y = hauteur_ecran // 2 - joueur1_rect.height // 2 + 165
        jumping1 = False

    # Limite du saut du joueur 2 pour éviter un déplacement infini vers le haut
    if joueur2_rect.y >= hauteur_ecran // 2 - joueur2_rect.height // 2 + 165:
        joueur2_rect.y = hauteur_ecran // 2 - joueur2_rect.height // 2 + 165
        jumping2 = False
def gestion_touches_joueur2(touches_perso, touches, joueur2_rect, balle_rect, joueur_vitesse, jump_height, hauteur_ecran):
    # Gestion des déplacements du joueur 2 vers la gauche
    if touches[touches_perso[1]["Gauche"]] and joueur2_rect.top > 0 and not joueur2_rect.colliderect(balle_rect) and not joueur1_rect.colliderect(joueur2_rect):
        joueur2_rect.x -= joueur_vitesse

    # Gestion des déplacements du joueur 2 vers la droite
    if touches[touches_perso[1]["Droite"]] and joueur2_rect.bottom < hauteur_ecran and not joueur2_rect.colliderect(balle_rect) and not joueur1_rect.colliderect(joueur2_rect):
        joueur2_rect.x += joueur_vitesse
    if joueur1_rect.y >= hauteur_ecran // 2 - joueur1_rect.height // 2 + 165:
        joueur1_rect.y = hauteur_ecran // 2 - joueur1_rect.height // 2 + 165
        jumping1 = False

    # Limite du saut du joueur 2 pour éviter un déplacement infini vers le haut
    if joueur2_rect.y >= hauteur_ecran // 2 - joueur2_rect.height // 2 + 165:
        joueur2_rect.y = hauteur_ecran // 2 - joueur2_rect.height // 2 + 165
        jumping2 = False
def afficher_ecran(pos, ecran, fond, positions_balle, jambe_gauche, jambe_droite, jambe_gauche_rect, jambe_droite_rect,
                   joueur1, joueur2, balle, cage_gauche, cage_droite, texte_temps, texte_rect,
                   texte_score_joueur1, texte_score_joueur2, blanc, max_positions, largeur_ecran, joueur1_rect, joueur2_rect, balle_rect, cage_droite_rect, cage_gauche_rect):
    # Efface l'écran et affiche le fond
    ecran.fill(blanc)
    ecran.blit(fond, (0, 0))

    # Dessine des cercles représentant la trajectoire de la balle
    for i, (position, camp) in enumerate(positions_balle):
        radius = int((10 / max_positions) * i)
        color = (255, 77, 77, 50) if camp == "cote_gauche" else (50, 255, 255, 50)
        pygame.draw.circle(ecran, color, position, radius)

    # Animation des jambes des joueurs
    jambe_image, autre_jambe_image = (jambe_gauche, jambe_droite) if pos == "gauche" else (jambe_droite, jambe_gauche)
    jambe_rect, autre_jambe_rect = (jambe_droite_rect, jambe_gauche_rect) if pos == "droite" else (jambe_gauche_rect, jambe_droite_rect)

    for i in range(30):
        ecran.blit(pygame.transform.rotate(autre_jambe_image, i), autre_jambe_rect)
        ecran.blit(jambe_image, jambe_rect)

    # Appelle la fonction 'afficher' pour afficher les éléments du jeu
    afficher(ecran, joueur1, joueur2, balle, cage_gauche, cage_droite, texte_temps, texte_rect,
             texte_score_joueur1, texte_score_joueur2, largeur_ecran, joueur1_rect, joueur2_rect, balle_rect, cage_droite_rect, cage_gauche_rect)

def afficher(ecran, joueur1, joueur2, balle, cage_gauche, cage_droite, texte_temps, texte_rect,
             texte_score_joueur1, texte_score_joueur2, largeur_ecran, joueur1_rect, joueur2_rect, balle_rect, cage_droite_rect, cage_gauche_rect):
    # Affiche les images et les textes sur l'écran
    ecran.blit(joueur1, joueur1_rect)
    ecran.blit(joueur2, joueur2_rect)
    ecran.blit(balle, balle_rect)
    ecran.blit(cage_droite, cage_droite_rect)
    ecran.blit(cage_gauche, cage_gauche_rect)
    ecran.blit(texte_temps, texte_rect)

    # Affiche les scores aux positions spécifiées
    ecran.blit(texte_score_joueur1, (10, 10))
    ecran.blit(texte_score_joueur2, (largeur_ecran - 10 - texte_score_joueur2.get_width(), 10))

    # Met à jour l'affichage
    pygame.display.flip()

def gerer_saut_joueurs1(touches_perso, touches, joueur1_vitesse, hauteur_saut, en_saut1):
    # Gère le saut du joueur 1
    if touches[touches_perso[0]["Sauter"]] and not en_saut1:
        joueur1_vitesse = -hauteur_saut
        en_saut1 = True
    if joueur1_rect.y >= hauteur_ecran // 2 - joueur1_rect.height // 2 + 165:
        joueur1_rect.y = hauteur_ecran // 2 - joueur1_rect.height // 2 + 165
        jumping1 = False

    # Limite du saut du joueur 2 pour éviter un déplacement infini vers le haut
    if joueur2_rect.y >= hauteur_ecran // 2 - joueur2_rect.height // 2 + 165:
        joueur2_rect.y = hauteur_ecran // 2 - joueur2_rect.height // 2 + 165
        jumping2 = False
    return joueur1_vitesse, en_saut1
def gerer_saut_joueurs2(touches_perso, touches, joueur2_vitesse, hauteur_saut, en_saut2):
    # Gère le saut du joueur 2
    if touches[touches_perso[1]["Sauter"]] and not en_saut2:
        joueur2_vitesse = -hauteur_saut
        en_saut2 = True
    if joueur1_rect.y >= hauteur_ecran // 2 - joueur1_rect.height // 2 + 165:
        joueur1_rect.y = hauteur_ecran // 2 - joueur1_rect.height // 2 + 165
        jumping1 = False

    # Limite du saut du joueur 2 pour éviter un déplacement infini vers le haut
    if joueur2_rect.y >= hauteur_ecran // 2 - joueur2_rect.height // 2 + 165:
        joueur2_rect.y = hauteur_ecran // 2 - joueur2_rect.height // 2 + 165
        jumping2 = False
    # Rtourne les vitesses mises à jour et l'état de saut des joueurs
    return joueur2_vitesse, en_saut2


def activer_tir_parabolique(touches_perso, touches, tir_parabolique_active, en_saut, joueur_rect, balle_rect, vitesse_x, vitesse_y, angle_tir, vitesse_tir):
    # Conversion de l'angle de tir en radians
    angle_tir_rad = math.radians(angle_tir)

    # Activation du tir parabolique vers le bas ou à gauche
    if (touches[touches_perso[0]["Tir haut"]] or touches[touches_perso[0]["Tir bas"]]) and not tir_parabolique_active:
        if (
            abs(joueur_rect.centerx - balle_rect.centerx) < 50
            and abs(joueur_rect.centery - balle_rect.centery)<150
        ):
            tir_parabolique_active = True
            vitesse_x = vitesse_tir * math.sin(angle_tir_rad) + 12
            vitesse_y = -vitesse_tir * (4 if touches[touches_perso[0]["Tir haut"]] else 1) * math.sin(angle_tir_rad)

    # Activation du tir parabolique vers le haut ou à droite
    if (touches[touches_perso[1]["Tir haut"]] or touches[touches_perso[1]["Tir bas"]]) and not tir_parabolique_active:
        print(joueur_rect.y ,balle_rect.y)
        if (
            abs(joueur_rect.centerx - balle_rect.centerx) < 50
            and abs(joueur_rect.y - balle_rect.y)<150
        ):
            tir_parabolique_active = True
            vitesse_x = -vitesse_tir * math.sin(angle_tir_rad) - 12
            vitesse_y = -vitesse_tir * (4 if touches[touches_perso[1]["Tir haut"]] else 1) * math.sin(angle_tir_rad)
    if joueur1_rect.y >= hauteur_ecran // 2 - joueur1_rect.height // 2 + 165:
        joueur1_rect.y = hauteur_ecran // 2 - joueur1_rect.height // 2 + 165
        jumping1 = False

    # Limite du saut du joueur 2 pour éviter un déplacement infini vers le haut
    if joueur2_rect.y >= hauteur_ecran // 2 - joueur2_rect.height // 2 + 165:
        joueur2_rect.y = hauteur_ecran // 2 - joueur2_rect.height // 2 + 165
        jumping2 = False
    # Retourne l'état du tir parabolique et les nouvelles vitesses
    return tir_parabolique_active, vitesse_x, vitesse_y

def gestion_tir_parabolique(tir_parabolique_active, vitesse_x, vitesse_y, gravite, joueur_collision, max_y_rebond_precedent, balle_rect, joueur_rect, autre_joueur_rect, distance_autre_joueur_balle, difference_top_bottom_autre_joueur, dist, distance_joueur_balle, balle_vitesse, hauteur_ecran, dist2, tir_parabolique_joueur1_active):
    # Si le tir parabolique est actif
    if tir_parabolique_active:
        # Déplace la balle en fonction de la vitesse horizontale et verticale
        balle_rect.x += vitesse_x
        balle_rect.y += vitesse_y
        vitesse_y += gravite

        # Gestion des collisions avec l'autre joueur
        if distance_autre_joueur_balle < 60 and difference_top_bottom_autre_joueur < 55:
            vitesse_x = 0
            vitesse_y = 0
            tir_parabolique_active = False

        # Gestion du rebond au sol
        if balle_rect.bottom >= hauteur_ecran // 2 - balle_rect.height // 2 + 200:
            balle_rect.bottom = hauteur_ecran // 2 - balle_rect.height // 2 + 200
            vitesse_y = -vitesse_y * 0.5

            # Gestion des rebonds successifs
            difference_rebond = balle_rect.y - max_y_rebond_precedent
            if abs(difference_rebond) < 10:
                vitesse_x += 0.5
                vitesse_y += 0.5
                if abs(vitesse_x) < 6.5:
                    vitesse_x, vitesse_y = 0, 0
                    tir_parabolique_active = False

            # Gestion des rebonds avec les joueurs
            if (
                (dist < 10 and distance_joueur_balle < 15 and not tir_parabolique_joueur1_active and not tir_parabolique_joueur2_active) or
                (balle_vitesse[1] > 32 and dist < 30 and distance_joueur_balle < 15 and not tir_parabolique_joueur1_active and not tir_parabolique_joueur2_active) or
                (dist2 < 10 and distance_autre_joueur_balle < 15 and not tir_parabolique_joueur1_active and not tir_parabolique_joueur2_active) or
                (balle_vitesse[1] > 32 and dist2 < 30 and distance_autre_joueur_balle < 15 and not tir_parabolique_joueur1_active and not tir_parabolique_joueur2_active)
            ):
                bounce_coefficient = 0.5
                balle_vitesse[1] = -abs(balle_vitesse[1] * bounce_coefficient)

            max_y_rebond_precedent = max(balle_rect.y, max_y_rebond_precedent)
    if joueur1_rect.y >= hauteur_ecran // 2 - joueur1_rect.height // 2 + 165:
        joueur1_rect.y = hauteur_ecran // 2 - joueur1_rect.height // 2 + 165
        jumping1 = False

    # Limite du saut du joueur 2 pour éviter un déplacement infini vers le haut
    if joueur2_rect.y >= hauteur_ecran // 2 - joueur2_rect.height // 2 + 165:
        joueur2_rect.y = hauteur_ecran // 2 - joueur2_rect.height // 2 + 165
        jumping2 = False
    # Retourne l'état du tir parabolique et les nouvelles vitesses
    return tir_parabolique_active, vitesse_x, vitesse_y, max_y_rebond_precedent


def distance_entre_joueurs(joueur1_rect, joueur2_rect):
    return abs(joueur1_rect.centerx - joueur2_rect.centerx)

def afficher_ecran_mi_temps():
    # Afficher un écran de mi-temps ou effectuer des actions spécifiques à la mi-temps
    print("Mi-Temps! Pause avant la deuxième période.")
    time.sleep(5)  # Pause de 5 secondes pour l'exemple



def reinitialiser_jeu():
    # Réinitialisation des positions des joueurs
    balle_rect.center = (largeur_ecran // 2, hauteur_ecran // 2)
    tir_parabolique_joueur1_active = False
    tir_parabolique_joueur2_active = False
    # Réinitialiser la position des joueurs
    joueur1_rect = joueur1.get_rect(topleft=(50, hauteur_ecran // 2 - joueur1.get_height() // 2))
    joueur2_rect = joueur2.get_rect(topleft=(largeur_ecran - 50 - joueur2.get_width(), hauteur_ecran // 2 - joueur2.get_height() // 2))
    # Réinitialiser les scores si nécessaire
    score_joueur1, score_joueur2 = 0, 0
    return score_joueur1, score_joueur2, temps_de_jeu_restant, joueur2_rect,joueur1_rect,tir_parabolique_joueur1_active,tir_parabolique_joueur2_active,balle_rect