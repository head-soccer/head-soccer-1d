import pygame, math

def afficher_ecran_demarrage(intro, temps_de_demarrage, nom_jeu_texte, nom_jeu_rect, createurs_texte, createurs_rect):
    clock = pygame.time.Clock()
    temps_actuel = pygame.time.get_ticks()

    while pygame.time.get_ticks() - temps_actuel < temps_de_demarrage:
        for r in pygame.event.get():
            if r.type == pygame.QUIT:
                pygame.quit()

        intro.fill((0, 0, 0))  # Fond noir

        intro.blit(nom_jeu_texte, nom_jeu_rect)
        intro.blit(createurs_texte, createurs_rect)

        pygame.display.flip()
        clock.tick(60)
