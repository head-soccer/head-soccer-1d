# Head Soccer 1D



# Démarrer

Pour commencer, clonez le dépôt GitLab sur votre machine locale en utilisant la commande suivante :
    git clone https://gitlab.com/head-soccer/head-soccer-1d.git

## Installation des Dépendances:
Assurez-vous d'avoir Python installé sur votre machine. Ensuite, installez les dépendances requises en exécutant la commande suivante dans votre terminal: pip install -r requirements.txt

## Lancer le Jeu
Une fois les dépendances installées, vous pouvez lancer le jeu en exécutant le fichier principal du jeu : python main.py

Cela lancera le jeu et vous pourrez commencer à jouer !

# Nom
Head Soccer 1D

# Description
Head Soccer 1D est un jeu de football captivant qui revisite le plaisir du football rétro avec une touche moderne. Développé par une équipe d'étudiants passionnés de l'établissement Notre Dame de la Compassion, ce jeu offre une expérience unique où la nostalgie rencontre l'innovation.

Plongez dans l'univers vibrant de Head Soccer 1D et découvrez des graphismes rétro élégants associés à des fonctionnalités de jeu innovantes. Choisissez votre équipe favorite, personnalisez vos touches et affrontez des adversaires dans des matchs palpitants en un contre un.

Que vous soyez un amateur de football chevronné ou un nouveau venu dans le monde du jeu, Head Soccer 1D offre une expérience accessible et divertissante pour tous les joueurs. Préparez-vous à des moments de jeu passionnants, des duels épiques et des heures de plaisir avec Head Soccer 1D !

# Badges
[![Python Version](https://img.shields.io/badge/python-3.8-blue.svg)](https://www.python.org/downloads/release/python-380/)
[![Contributors](https://img.shields.io/badge/all_contributors-3-orange.svg?style=flat-square)](#contributors)

# Usage
Head Soccer 1D est facile à utiliser et offre une expérience de jeu immersive. Voici comment commencer :

1. Menu Principal
Au lancement du jeu, vous serez accueilli par le menu principal. Utilisez les touches directionnelles pour naviguer dans le menu et appuyez sur la touche Entrée pour sélectionner une option.

2. Personnalisation
Dans le menu de personnalisation, vous pouvez configurer les options du jeu telles que les touches de contrôle et les préférences de l'équipe. Suivez les instructions à l'écran pour personnaliser votre expérience de jeu.

3. Choix de Mode
Head Soccer 1D propose différents modes de jeu, y compris le mode solo contre l'IA et le mode multijoueur. Sélectionnez le mode de jeu de votre choix dans le menu principal pour commencer à jouer.

4. Jouer au Jeu
Une fois dans le jeu, utilisez les touches configurées pour contrôler votre joueur. Affrontez vos adversaires et marquez des buts pour remporter la victoire !

5. Fin de Partie
À la fin de chaque partie, vous verrez un récapitulatif de votre performance. Appuyez sur une touche pour revenir au menu principal et explorer d'autres options de jeu.

# Roadmap
## Version Actuelle : 1.0.0
Implémentation des fonctionnalités de base du jeu.
Personnalisation des touches et des équipes.
Modes de jeu solo et multijoueur disponibles.
## Prochaines Étapes : 1.1.0
Ajout de nouveaux modes de jeu.
Amélioration de l'IA des adversaires.
Optimisation des performances du jeu.
## Futur : 2.0.0
Intégration de fonctionnalités en ligne pour des matchs multijoueurs en réseau.
Ajout de nouveaux personnages et équipes.
Mises à jour régulières avec du contenu supplémentaire et des corrections de bugs.
## Idées Potentielles :
Création d'un éditeur de niveaux pour permettre aux joueurs de créer leurs propres terrains de jeu.
Intégration de fonctionnalités de réalité virtuelle pour une expérience de jeu encore plus immersive.
Développement de versions mobiles pour une accessibilité accrue.

# Support
Si vous avez besoin d'aide ou si vous avez des questions concernant Head Soccer 1D, n'hésitez pas à nous contacter :

Email : headsoccer1d.sup@gmail.com
# Auteur 
Nous tenons à remercier les personnes suivantes qui ont contribué à la réalisation de Head Soccer 1D :

Khaled Boujelben : Responsable de la programmation
Yassine El Ouadi : Directeur artistique
Chady Mortady : Chef de projet
Nous exprimons notre gratitude à toute notre équipe pour son travail acharné et son engagement à faire de ce projet une réalité. Merci également à la communauté des développeurs et à tous ceux qui ont apporté leur soutien et leurs idées tout au long du processus de développement.

# License
Ce projet est sous licence  libre GPL v3+ et le texte sous licence Creative Commons CC By-Sa

# État d'avancement du projet
Note : Le développement de ce projet avance régulièrement. Nous sommes ouverts aux contributions et nous accueillons avec enthousiasme toute aide de la part de la communauté. Si vous êtes intéressé à contribuer ou à participer au développement, n'hésitez pas à nous contacter. Votre soutien est précieux pour faire avancer ce projet !